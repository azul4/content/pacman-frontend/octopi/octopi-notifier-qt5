# octopi-notifier-qt5

Notifier for Octopi using Qt5 libs

https://tintaescura.com/projects/octopi/

https://github.com/aarnt/octopi

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/pacman-frontend/octopi/octopi-notifier-qt5.git
```
